%global _empty_manifest_terminate_build 0
Name:		python-eli5
Version:	0.10.1
Release:	1
Summary:	Debug machine learning classifiers and explain their predictions
License:	MIT license
URL:		https://github.com/TeamHG-Memex/eli5
Source0:	https://files.pythonhosted.org/packages/05/dc/ae333ca238bf3809164f6dfef42f75d2199287f1db7c93425db6c1f4af7d/eli5-0.10.1.tar.gz
BuildArch:	noarch

Requires:	python3-attrs
Requires:	python3-jinja2
Requires:	python3-numpy
Requires:	python3-scipy
Requires:	python3-six
Requires:	python3-scikit-learn
Requires:	python3-graphviz
Requires:	python3-tabulate
Requires:	python3-typing
Requires:	python3-singledispatch

%description


%package -n python3-eli5
Summary:	Debug machine learning classifiers and explain their predictions
Provides:	python-eli5
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-eli5


%package help
Summary:	Development documents and examples for eli5
Provides:	python3-eli5-doc
%description help


%prep
%autosetup -n eli5-0.10.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-eli5 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 14 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
